"""
simulate dollar cost averaging per day
"""

from datetime import datetime, timedelta

from lib.extract import parse_csv
from lib.extract import row_mapper
from lib.strategy.dollar_avg import run_strategy

import sys

if __name__ == '__main__':
    if len(sys.argv) < 5:
        print("""
    Args: (5 args required)
    Implement dollar cost avg strategy. The strategy is:

        * invest in fixed increments every "period"
        * an increment is an (amount, rate) pair. the value amount x rate
            stays "fixed" for each period, so if the price rises, you buy
            fewer items; if the price drops, you buy more

    :param coin type: eth / btc / ltc 
    :param capital: total amount you have to invest
    :param start_date: start_date for investing
    :param end_date: end_date for investing
    :param delta: time period over which to spread investments
""")
        sys.exit(0)

    capital_usd = float(sys.argv[2]) 
    start_dt = datetime.strptime(sys.argv[3], "%Y-%m-%d").date()
    end_dt = datetime.strptime(sys.argv[4], "%Y-%m-%d").date()
    d = timedelta(days=int(sys.argv[5]))

    data = list(map(row_mapper, parse_csv("data/%s.csv" % sys.argv[1])))
    max_val = run_strategy(capital_usd, start_dt, end_dt, d, data)

    print("""\nWith an investment of %.2f between %s and %s every %s days,
    you could have made a max of $%.2f with dollar cost averaging.""" %
        (capital_usd, start_dt, end_dt, d.days, max_val))
