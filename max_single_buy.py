"""
simulate dollar cost averaging per day
"""

from datetime import datetime

from lib.extract import parse_csv
from lib.extract import row_mapper
from lib.strategy.max_single_buy import run_strategy

import sys

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print("""
    Args: (4 args required)
    Implement max single buy. The strategy is:

    * make a one-time buy, one-time sell
    * limited to a time window

    :param coin type: eth / btc / ltc 
    :param capital: total amount you have to invest
    :param start_date: start_date for investing
    :param end_date: end_date for investing
""")
        sys.exit(0)

    capital_usd = float(sys.argv[2]) 
    start_dt = datetime.strptime(sys.argv[3], "%Y-%m-%d").date()
    end_dt = datetime.strptime(sys.argv[4], "%Y-%m-%d").date()

    data = list(map(row_mapper, parse_csv("data/%s.csv" % sys.argv[1])))
    max_val = run_strategy(capital_usd, start_dt, end_dt, data)
    print("""With an investment of %.2f between %s and %s,
    you could have made a max of $%.2f with max single buy.""" %
        (capital_usd, start_dt, end_dt, max_val))
