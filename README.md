# sparrow

study on trading strategies for cryptos and how much you could have made ;( It's interesting to try these with different dates across strategies and currencies.

note that these programs error out if the dates and deltas are not specified correctly, so if it errors out, try expanding your date period.

## setup

Make sure you have Python2.7+

```
python --version
```

Then run the trading strategy of your choice on the top level folder:

```
python dollar_cost_averaging.py ltc 10000.0 2017-10-19 2017-11-21 7
```

For what args mean, type:

```
python dollar_cost_averaging.py
```

## strategies

### dollar cost averaging

Invest a fixed amount each period

Example:

```
python dollar_cost_averaging.py eth 10000.0 2017-11-1 2017-11-21
```

### max single buy

Invest all at once, buy / sell optimally.

Example:

```
python max_single_buy.py btc 10000.0 2017-11-1 2017-11-21
```
