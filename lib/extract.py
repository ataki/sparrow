from datetime import datetime
import csv

key_mapper = {
    "date": lambda x: datetime.strptime(x, "%Y-%m-%d").date(),
    "txVolume": lambda x: float(x),
    "txCount": lambda x: float(x),
    "marketcap": lambda x: float(x),
    "price": lambda x: float(x),
    "exchangeVolume": lambda x: float(x),
    "generatedCoins": lambda x: float(x),
    "fees": lambda x: float(x),
}

def row_mapper(row):
    kvs = []
    for k, fn in key_mapper.items():
        kvs.append((k, fn(row[k])))
    return dict(kvs)

def parse_csv(fname):
    fieldnames = [
        "date",
        "txVolume",
        "txCount",
        "marketcap",
        "price",
        "exchangeVolume",
        "generatedCoins",
        "fees",
        "extras"
    ]

    with open(fname) as f:
        # advance past headers
        f.readline()
        reader = csv.DictReader(f, fieldnames=fieldnames)
        for record in reader:
            yield record
