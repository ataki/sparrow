from datetime import timedelta

def date_range(start_date, end_date, period):
    n = (end_date - start_date) // period
    for i in range(int(n)):
        yield start_date + timedelta(i)