from lib.utils import date_range

"""
Implement reinvesting; if we shouldn't invest
at all, return a negative value to signal to
the caller that we should accumulate $ for the
next period
"""
def get_amount_to_invest_at_date(capital, buy_price):
    if buy_price > capital:
        return 0.0
    else:
        return capital / buy_price

def get_profits_at_opt_sell_date(buy_date, buy_price, buy_amt, price_data):
    cands = list(filter(lambda x: x[0] >= buy_date, price_data))
    i_argmax = max(enumerate(cands), key=lambda c: c[1])
    date_price = cands[i_argmax[0]]

    # if price is only decreasing
    if date_price[1] <= buy_price:
        return (buy_date, 0.0)
    else:
        return (date_price[0], buy_amt * (date_price[1] - buy_price))

"""
Implement dollar cost avg strategy. The strategy is:

    * invest in fixed increments every "period"
    * an increment is an (amount, rate) pair. the value amount x rate
        stays "fixed" for each period, so if the price rises, you buy
        fewer items; if the price drops, you buy more

:param capital: float, total amount you have to invest
:param start_date: datetime.datetime, start_date for investing
:param end_date: datetime.datetime, end_date for investing
:param delta: datetime.timedelta, time period over which to spread investments
:param data: List[Dict], exchange data. See `lib/extract` for format

:return amount you would have made pre-tax
"""
def run_strategy(capital, start_date, end_date, period, data):
    price_data = [(x["date"], x["price"]) for x in data]
    ordered_price_data = sorted(price_data, key=lambda x: x[0])
    date_for_price = dict(ordered_price_data)

    n = (end_date - start_date) // period
    investment_per_period = capital // n

    investments_by_date = [(dt, investment_per_period)
        for dt in date_range(start_date, end_date, period)]

    max_profits = 0
    reserved_capital = 0
    for i, el in enumerate(investments_by_date):
        buy_date, investment = (el[0], el[1] + reserved_capital)
        buy_price = date_for_price[buy_date]
        buy_amount = get_amount_to_invest_at_date(investment, buy_price)

        # recompute savings for each period
        if buy_amount == 0.0:
            reserved_capital += investment
        else:
            reserved_capital = 0

        sell_date, profit = get_profits_at_opt_sell_date(buy_date, buy_price, buy_amount, ordered_price_data)
        print("bought %s on %s at %s, sold at %s for %f" % (buy_amount, buy_date, buy_price, sell_date, profit))
        max_profits += profit
    return max_profits
