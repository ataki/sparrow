"""
Implement max single. The strategy is:

    * make a one-time buy, one-time sell
    * limited to a time window

:param capital: float, total amount you have to invest
:param start_date: datetime.datetime, start_date for investing
:param end_date: datetime.datetime, end_date for investing
:param raw_data: List[Dict], exchange data. See `lib/extract` for format

:return float, amount you would have made pre-tax
"""
def run_strategy(capital, start_date, end_date, raw_data):
    price_data = [(x["date"], x["price"]) for x in raw_data
        if x["date"] >= start_date and x["date"] < end_date]
    ordered_price_data = sorted(price_data, key=lambda x: x[0])

    buy_date = None
    sell_date = None
    max_profits_so_far = 0
    min_so_far = ordered_price_data[0][1]

    for el in ordered_price_data:
        date, price = el
        if price < min_so_far:
            buy_date = date
            min_so_far = price

        shares_to_buy = capital / min_so_far
        profit = shares_to_buy * (price - min_so_far)

        if max_profits_so_far < profit:
            sell_date = date
            max_profits_so_far = profit

    print("bought on %s, sold on %s" % (buy_date, sell_date))
    return max_profits_so_far
